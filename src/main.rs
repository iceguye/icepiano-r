//  Copyright © 2022 - 2024 IceGuye.

// Unless otherwise noted, all files in this project, including but
// not limited to source codes and art contents, are free software:
// you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software
// Foundation, version 3 of the License.

// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

mod notes_manager;
mod midi_input;
mod utilities;
mod window;
mod device_selector;

use std::process::exit;
use std::fs;
use sdl2::mixer;
use sdl2::mixer::{AUDIO_F32LSB};
use std::thread;
use std::sync::{Arc, RwLock};
use crate::midi_input::MidiRunner;
use crate::notes_manager::{Sound, PlayNotes, stop_notes};
use crate::window::display_window;

pub const CHANNEL_SIZE: usize = 1024;

fn main() {
    // Select a midi channel, this program uses midi channel 1 defaultly.
    let mut selected_channel: u8 = 1;
    
    // Ask the user to select a midi input device from a list.
    let mut midi1 = MidiRunner::new();
    match midi1.input_select() {
        Ok(_) => (),
        Err(err) => {
            println!("Error: {}", err);
            exit(1);         
        }
    }
    
    // Spawn a thread to run the midi reader, and share memory to the
    // mean loop.
    let midi1_bridge = Arc::new(RwLock::new([0, 0, 0, 0]));
    let midi1_bridge_ct = Arc::clone(&midi1_bridge);
    thread::spawn (move || {
        match midi1.run_reader(midi1_bridge_ct) {
            Ok(_) => (),
            Err(err) => println!("Error: {}", err)
        }
    });

    // Define some mutable variable for the main loop.
    let mut express_pedal: u8 = 127;
    let mut sustain_pedal: u8 = 0;
    let mut mid_pedal: u8 = 0;
    let mut mid_pedal_hold: [bool; 128] = [false; 128];
    let mut keys_down: [u8; 128] = [0; 128];
    let mut keys_down2: Vec<u8> = Vec::new();
    let mut old_received = [0, 0, 0, 0];
    let mut midi_poll: bool;

    // Spawn the graphical window system.
    let exit_bridge = Arc::new(RwLock::new(false));
    let exit_bridge_ct = Arc::clone(&exit_bridge);
    let keys_bridge = Arc::new(RwLock::new(keys_down));
    let keys_bridge_ct = Arc::clone(&keys_bridge);
    let pedals_bridge = Arc::new(RwLock::new((sustain_pedal,
                                             express_pedal,
                                             mid_pedal)));
    let pedals_bridge_ct = Arc::clone(&pedals_bridge);
    let selected_channel_rcv = Arc::new(RwLock::new(selected_channel));
    let selected_channel_snd = Arc::clone(&selected_channel_rcv);

    let initiating_a = Arc::new(RwLock::new(0));
    let initiating_a_cl = initiating_a.clone();
    thread::spawn (move || {
        display_window(exit_bridge_ct,
                       keys_bridge,
                       pedals_bridge,
                       selected_channel_snd,
		       initiating_a_cl);
    });
    
    // Build SDL2_mixer channel.
    mixer::open_audio(48000, AUDIO_F32LSB, 2, 1024).unwrap();
    mixer::allocate_channels(CHANNEL_SIZE as i32);
    let mut channel_notes: [usize; CHANNEL_SIZE] = [0; CHANNEL_SIZE];

    // Building sound and note structures.    
    let processed_samples_paths = 
	fs::read_dir("./data/processed-samples")
        .unwrap();
    let paths_count = processed_samples_paths.count();
    let processed_samples_paths = 
	fs::read_dir("./data/processed-samples")
        .unwrap();
    let mut play_notes = PlayNotes::new();
    let mut fc = 0;
    for path in processed_samples_paths {
        fc = fc + 1;
        let path_string = path.unwrap().path().display().to_string();
        let path_str = path_string.as_str();
        let load_sound = Sound::new(path_str);
        println!("Load sound: {} ... {}", path_str, fc);
        let note = load_sound.note;
        play_notes.notes[note].as_mut().unwrap().add_sound(load_sound);
	let mut initiating_w = initiating_a.write().unwrap();
	*initiating_w = (fc as f64 / paths_count as f64 * 100.0) as usize ;
	drop(initiating_w);
    }
    let mut initiating_w = initiating_a.write().unwrap();
    *initiating_w = 101;
    drop(initiating_w);

    // Define the multi-theading Arc-RwLock bridgs for stoppers
    // threads.
    let keys_bridge2 = Arc::new(RwLock::new(keys_down));
    let keys_bridge2_ct = Arc::clone(&keys_bridge2);
    let channel_notes_bridge = Arc::new(RwLock::new(channel_notes));
    let channel_notes_bridge_ct = Arc::clone(&channel_notes_bridge);
    let mid_pedal_bridge = Arc::new(RwLock::new(mid_pedal_hold));
    let mid_pedal_bridge_ct = Arc::clone(&mid_pedal_bridge);
    let sustain_pedal_bridge = Arc::new(RwLock::new(sustain_pedal));
    let sustain_pedal_bridge_ct = Arc::clone(&sustain_pedal_bridge);
    let cur_channel_loop_rcv = Arc::new(RwLock::new(0));
    let cur_channel_loop_snd = Arc::clone(&cur_channel_loop_rcv);
    thread::spawn(move || {
        stop_notes(&channel_notes_bridge,
                   &keys_bridge2,
                   &mid_pedal_bridge,
                   &sustain_pedal_bridge,
                   &cur_channel_loop_rcv);
    });
    
    // Start the main loop
    loop {
        let should_exit = *exit_bridge.read().unwrap();
        if should_exit == true {
            break;
        }
        let new_received = *midi1_bridge.read().unwrap();
        if new_received != old_received {
            midi_poll = true;
            old_received = new_received;
        } else {
            midi_poll = false;
        }
        selected_channel = *selected_channel_rcv.read().unwrap();
        if midi_poll {
            println!("Shared midi message received... {:?}", new_received);
            // Add midi related codes here.

            // Define the variables to store those already received
            // midi information.
            let midi_type = new_received[1] as u8;
            let midi_note = new_received[2] as u8;
            let midi_valu = new_received[3] as u8;

            // Pedals midi processing.
            if midi_type == 176 && midi_note == 64 {
                sustain_pedal = midi_valu;
                let mut sustain_pedal_sender = sustain_pedal_bridge_ct
                    .write()
                    .unwrap();
                *sustain_pedal_sender = sustain_pedal;
		drop(sustain_pedal_sender);
            }
            if midi_type == 176 && midi_note == 7 {
                express_pedal = midi_valu;
            }
            if midi_type == 176 && midi_note == 1 {
                mid_pedal = midi_valu;
                if midi_valu > 0 {
                    for i in &keys_down2 {
                        mid_pedal_hold[*i as usize] = true;
                    }
                } else {
                     mid_pedal_hold = [false; 128];
                }
                let mut mid_pedal_sender = mid_pedal_bridge_ct.write().unwrap();
                *mid_pedal_sender = mid_pedal_hold;
		drop(mid_pedal_sender);
            }
            if midi_type == 176 {
                let pedals: (u8, u8, u8) = (sustain_pedal,
                                            express_pedal,
                                            mid_pedal);
                let mut pedals_sender = pedals_bridge_ct.write().unwrap();
                *pedals_sender = pedals;
		drop(pedals_sender);
            }
            
            // Keys midi processing.
            let keyup = (midi_type == 144 - 1 + selected_channel
                         && midi_valu == 0)
                || (midi_type == 128 - 1 + selected_channel);
            let keydown = midi_type == 144  - 1 + selected_channel
                && midi_valu > 0;
            if keyup {
                keys_down[midi_note as usize] = midi_valu;
                let mut keys_sender = keys_bridge_ct.write().unwrap();
                *keys_sender = keys_down;
		drop(keys_sender);
                let mut keys_sender2 = keys_bridge2_ct.write().unwrap();
                *keys_sender2 = keys_down;
		drop(keys_sender2);
                keys_down2.retain(|&x| x != midi_note);
            }
            if keydown {
                keys_down[midi_note as usize] = midi_valu;
                keys_down2.push(midi_note);
                let mut keys_sender = keys_bridge_ct.write().unwrap();
                *keys_sender = keys_down;
		drop(keys_sender);
                let mut keys_sender2 = keys_bridge2_ct.write().unwrap();
                *keys_sender2 = keys_down;
		drop(keys_sender2);
                play_notes.play(midi_note as usize, midi_valu as u8, express_pedal, &mut channel_notes);
                let mut cur_channel_loop_sender = cur_channel_loop_snd
                    .write()
                    .unwrap();
                *cur_channel_loop_sender = play_notes.channel_loop;
		drop(cur_channel_loop_sender);
                let mut channel_notes_sender = channel_notes_bridge_ct
                    .write()
                    .unwrap();
                *channel_notes_sender = channel_notes;
		drop(channel_notes_sender);
            }
        }
    }
}
