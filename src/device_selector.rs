//  Copyright © 2024 IceGuye.

// Unless otherwise noted, all files in this project, including but
// not limited to source codes and art contents, are free software:
// you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software
// Foundation, version 3 of the License.

// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

use sdl2;
use sdl2::keyboard::Keycode;
use sdl2::event::Event;
use std::sync::{Arc, RwLock};
use sdl2::ttf;
use sdl2::pixels::Color;
use sdl2::rect::Rect;

pub fn display_device_selector(port_ls: &[String],
			       midi_input_a: Arc<RwLock<String>>) {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem.window(
	"IcePiano-R Midi Device Selection", 800, 600)
        .build()
        .unwrap();
    let mut canvas = window.into_canvas().build().unwrap();
    let texture_creator = canvas.texture_creator();
    let ttf_ctx = ttf::init().unwrap();
    let font_size = 24;
    let font = ttf_ctx.load_font(
	"./data/fonts/NotoSansMono-Regular.ttf", font_size)
	.unwrap();
    let instruction_1 = String::from(
	"You have following midi devices available: ");
    let instruction_2 = String::from(
	"Please select one of them by pressing number key."
    );
    let instr_size_1 = font.size_of(&instruction_1).unwrap();
    let instr_size_2 = font.size_of(&instruction_2).unwrap();
    let instruction_surface_1 = font
        .render(&instruction_1)
        .blended(Color::RGBA(0, 0, 0, 255))
        .unwrap();
    let instruction_surface_2 = font
        .render(&instruction_2)
        .blended(Color::RGBA(0, 0, 0, 255))
        .unwrap();
    let inst_texture_1 = texture_creator
	.create_texture_from_surface(&instruction_surface_1)
	.unwrap();
    let inst_texture_2 = texture_creator
	.create_texture_from_surface(&instruction_surface_2)
	.unwrap();
    canvas.set_draw_color(Color::RGBA(195, 217, 255, 255));
    canvas.clear();
    canvas.copy(&inst_texture_1,
		None,
		Rect::new(0, 0, instr_size_1.0, instr_size_1.1)).unwrap();
    for (i, port) in port_ls.iter().enumerate() {
	let text_size = font.size_of(port).unwrap();
	let text_surface = font.render(port)
	    .blended(Color::RGBA(0, 0, 0, 255))
	    .unwrap();
	let texture = texture_creator
	    .create_texture_from_surface(&text_surface)
	    .unwrap();
	let padding = font_size as usize * (i + 1);
	canvas.copy(&texture,
		    None,
		    Rect::new(0, padding as i32, text_size.0, text_size.1))
	    .unwrap();
    }

    canvas.copy(&inst_texture_2,
		None,
		Rect::new(0, 300, instr_size_2.0, instr_size_2.1)).unwrap();
    canvas.present();
    let mut event_pump = sdl_context.event_pump().unwrap();
    'running: loop{
	for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    break 'running
                },
		Event::TextInput {text, ..} => {
		    let mut midi_input_w = midi_input_a.write().unwrap();
		    *midi_input_w = text;
		    drop(midi_input_w);
		    break 'running
		},
                _ => {}
            }
        }
    }
}
