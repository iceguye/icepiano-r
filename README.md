# IcePiano-R

## Introduction

This program is a free and open source virtual piano written in Rust
programming language. It can be used to generate piano sounds for midi
keyboards (midi controllers), such as M-Audio Hammer 88.

## Download and Use

Linux binary package can be downloaded from here:
<https://gitlab.com/iceguye/icepiano-r-binary-release/-/archive/0.1.1-x86_64_linux/icepiano-r-binary-release-0.1.1-x86_64_linux.tar.gz>. No
installation, no dependencies, no BS. You should be able to run it on
most common Linux distributions by just double clicking the
icepiano-r.

## Building from source

Assume that you have a newly installed Linux operating system, the
following tools and development packages are needed:

git >= 2.44.0

git-lfs >= 3.4.1

gcc >= 14.0.1

gcc-c++ >= 14.0.1

SDL2 >= 2.0.8

SDL2_mixer >= 2.0.4

SDL2_image >= 2.0.5

SDL2_ttf >= 2.0.11

alsa-lib >= 1.2.11

jbigkit >= 2.1

liblerc >= 4.0.0

libmodplug >= 0.8.9.0

The listed tools and dependencies above were tested in a brand new
sandbox created by ToolBox on a Fedora Silverblue 40 machine.

On Fedora Linux, you can use the following command to install the
above packages:

    sudo dnf install git git-lfs gcc gcc-c++ SDL2-devel \
    SDL2_mixer-devel SDL2_image-devel SDL2_ttf-devel alsa-lib-devel \
    jbigkit-devel liblerc-devel libmodplug-devel

Install the latest official Rust toolchain (<https://rustup.rs/>):

    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

For the first time build:

    git clone https://gitlab.com/iceguye/icepiano-r
    cd icepiano-r
    cargo build --release

For updating to the latest version:

    cd icepiano-r
    git pull
    cargo build --release

## Run your build

Plug your USB midi keyboard to your computer.

Then run:

    cargo run --release

To change the midi channel, we use some sort of Emacs combination keys
style. This must be done while the window of IcePiano-R is active;
this cannot be done in your terminal, or the program may quit.

   ctrl+p MIDI_CHANNEL_NUMBER Return

## Authors and Contributors

- IceGuye

    -- Main developer, email: ice.guye@bk.ru

- Alexander Holm

    -- Who provided the samples freely, email: axeldenstore (at) gmail
       (dot) com

- Jiao Yidi

    -- Professional pianist, who provided research data for this
       project, especially for the stoppers' behaviors. Website:
       <https://jiaoyidi.art>

- Hao

    -- Physicist, provided huge supports and contributions to the
       program of perfect harmonic series.

(Disclaimer: People who are mentioned above do not neccessary know or
have met me, and they do not neccessary agree with me about
anything. They are the people whom I asked questions, sought for
helps, or I took their open source projects. Nothing more. )

## Contribution

Contributions are welcome in several ways.

1.  Bugs and other issues can be reported via
    <https://gitlab.com/iceguye/icepiano-r/-/issues>. Please be
    reminded that, follow-up comments, such as requests for more
    details, may be posted publicly. If it is one of your concerns,
    please read 3rd to 5th below.

2.  Significant bug fixes, features adds, and significant improvement
    can be done via submitting merge requests.
    (<https://gitlab.com/iceguye/icepiano-r/-/merge_requests>). Please
    please be reminded that, if you contribute via merge requests, and
    your merge requests do not meet the expectations of this project,
    they will be ignored or closed with no public comments. If you
    prefer a further discussion, please follow the 3rd below.

3.  For any minor, debatable, and controversial fixes or improvements,
    please send patches with your discussion or proposal via email:
    <ice.guye@bk.ru>. Your patch should include a detailed commit
    message that fully explain why this patch is needed and how it
    works, quotes and citations may be required if you got the ideas
    from others. The project will reply directly via email to let you
    know if your patches are used or not and why. You will be credited
    if your patches are merged. you can use following git command to
    create a patch for a commit:

        git format-patch COMMIT-SHA

4.  Any other questions can be sent via email: <ice.guye@bk.ru>.

5.  To protect your personal privacy and/or your fragile mentality,
    any information sent via the 3rd or 4th above will not be posted
    and forwarded to any 3rd parties nor public spaces. All the
    replies will be directly sent back to you. Thank you for your
    contributions.

## Copyright

Copyright © 2021-2024 IceGuye

Unless otherwise noted, all files in this project, including but NOT
limited to source codes and art contents, are free software: you can
redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, version 3
of the License.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.